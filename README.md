

## INSTALLATION

### Umziehen eines 'Pragmamx Module Forum SMF' auf eine Sub- oder Domain.
* Schwierigkeit: schwer
* Erforderliche Kenntnisse: FTP, PHP und MySQL
* Im Vorfeld brauchen wir FTP & DB Zugangsdaten der neuen Domain.
* Ein Backup wird nicht benötigt, da alle benötigten Dateien auf die neue Domain umgezogen werden. Und durch das Upgraden des Forums ein Backup ausgewählt werden kann.
* Bei der Installation wird eine PHP Version von <= 5.6.x benötigt, da SMF v2.0.13 auf PHP Version 7.x.x noch nicht läuft.


## Datenbank
1. Datenbank vom Forum in eine neue Datenbank kopieren.
3. Die Einstellungen müssen in der neuen Datenbank für das update geändert werden:
```
{$prefix}_themes 'theme_dir' => 'Themes/default'
{$prefix}_themes 'theme_url' => 'Themes/default'
{$prefix}_themes 'images_url' => 'Themes/default/images'
{$prefix}_settings 'attachmentUploadDir' => 'attachment'
```

## FTP / Dateien
1. Aktuelle SMF Version "Large upgrade" (http://download.simplemachines.org/) downloaden und per FTP auf die neue Domain uploaden.
2. Alle Dateien vom PMX Forum Ordner: "attachments", "avatars", "Smileys" hinzufügen.
3. Die leere 'Settings.php' und 'Settings_bak.php' ins Hauptverzeichnis kopieren.
4. "Repair Settings" von http://download.simplemachines.org/?tools ausführen. Dabei werden die Einstellungen in der Settings.php überprüft und bereinigt.
4.1. 'Set SMF Default theme as overall forum default for all users:' => 'Yes'
4.2. Der Prefix sollte mit der vom PMX Forum übereinstimmen.


## Upgrade
1. Das Update ausführen "./upgrade.php".
2. Man sieht den Fortschrittbalken, wie er sich bewegt.
2.1. Bei einem größerem Forum kann es eine Weile dauern.


## Fertig
1. Jetzt können weitere Einstellungen zum Forum getroffen werden (Plugins, Themes, Sprache etc.).

## PMX/SMF-url-repair-tool
TODO - Dieses Script hochladen
TODO - Konfigurieren
TODO - Ausführen
TODO - Datei löschen (automatisch per button)
TODO - copy folder "media/userpic/*"


## Information: Datenbank vom Forum
Die Datenbank Tabellen von PMX Module Forum 'SMF v1.1.21' (ohne prefix) inkl PMX Bridge.
```
 _attachments
 _ban_groups
 _ban_items
 _boards
 _board_permissions
 _calendar
 _calendar_holidays
 _categories
 _collapsed_categories
 _log_actions
 _log_activity
 _log_banned
 _log_boards
 _log_errors
 _log_floodcontrol
 _log_karma
 _log_mark_read
 _log_notify
 _log_online
 _log_polls
 _log_search_messages
 _log_search_results
 _log_search_subjects
 _log_search_topics
 _log_topics
 _membergroups
 _members
 _messages
 _message_icons
 _moderators
 _package_servers
 _permissions
 _personal_messages
 _pm_recipients
 _polls
 _poll_choices
 _sessions
 _settings
 _smileys
 _themes
 _topics
 ```
